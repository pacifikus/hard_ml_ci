import json
import os
import requests
from flask import Flask, jsonify, request
import time

app = Flask(__name__)
secret_number = os.get_environ["SECRET_NUMBER_URL"]


@app.route('/return_secret_number', methods=['GET'])
def run():
    return jsonify(secret_number=secret_number)


if __name__ == '__main__':
    app.run(debug=True, host='0.0.0.0', port=5000)
