FROM tiangolo/uwsgi-nginx-flask:python3.8
ARG SECRET_NUMBER_ARG

COPY . .

COPY requirements.txt requirements.txt
RUN pip install -r requirements.txt

ENV SECRET_NUMBER ${SECRET_NUMBER_ARG}

CMD [ "uwsgi"]